﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(los_controller.Startup))]
namespace los_controller
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
